#!/usr/bin/env python3
# coding --utf-8--

import cv2
import gym
import gym_simple_grid

import argparse
import importlib

from gym_simple_grid import helpers
from loguru import logger

keys = dict
actions = dict


def create_keys():
    global keys
    global actions
    keys = {
        '1': 49,
        '2': 50,
        '3': 51,
        '4': 52,
        '5': 53,
        '6': 54,
        '7': 55,
        '8': 56,
        '9': 57,
        '0': 48,
        'q': 113,
        'esc': 27,
        'left': 81,
        'up': 82,
        'right': 83,
        'down': 84,
    }
    actions = {
        keys['1']: helpers.Actions.Actions.DownLeft,
        keys['2']: helpers.Actions.Actions.Down,
        keys['3']: helpers.Actions.Actions.DownRight,
        keys['4']: helpers.Actions.Actions.Left,
        keys['5']: helpers.Actions.Actions.Stay,
        keys['6']: helpers.Actions.Actions.Right,
        keys['7']: helpers.Actions.Actions.UpLeft,
        keys['8']: helpers.Actions.Actions.Up,
        keys['9']: helpers.Actions.Actions.UpRight,
        keys['down']: helpers.Actions.Actions.Down,
        keys['left']: helpers.Actions.Actions.Left,
        keys['right']: helpers.Actions.Actions.Right,
        keys['up']: helpers.Actions.Actions.Up,
    }


def run_loop():
    global keys
    global actions

    for i in range(10000):
        img = env.render('rgb_array')

        cv2.imshow("First", img)

        if args.two_algo:
            img2 = env2.render('rgb_array')
            cv2.imshow("Second", img2)

        key = cv2.waitKey(0)

        if key in (keys['q'], keys['esc']):
            exit()
        try:
            action = actions[key]
        except KeyError:
            action = helpers.Actions.Actions.Stay
        obs, reward, done, info = env.step(action)

        if args.two_algo:
            obs2, reward2, done2, info2 = env2.step(action)
        logger.debug(f"\tGot reward: {reward}")
        if done:
            logger.info(f"Reward: {reward}")
            env.reset()


@logger.catch
def get_env_from_name(name_of_env: str) -> gym.Env:
    try:
        env = getattr(importlib.import_module("gym_simple_grid.envs"), name_of_env)
        logger.info(f"Loaded env {name_of_env}")

        return env()
    except Exception as e:
        logger.error(f" Can not import module. {e}")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("--env", required=False, default='RepeatedEnv',
                        help="name of the environment")
    parser.add_argument("--env_second", required=False, default='EnvWithSecondAgent',
                        help="name of the second environment")
    parser.add_argument("--seed", required=False, default=1337,
                        help="Value for Random generation")
    parser.add_argument("--two_algo", required=False, default=False,
                        help="Turn on two connected layers or not")
    args = parser.parse_args()

    env = get_env_from_name(args.env)
    env.seed(1337)

    if args.two_algo:
        try:
            env2 = get_env_from_name(args.env_second)

            env2.normal_init(env.second_agent_pose, env.action_space, env.grid, env._target, env.position,
                             env.grid_size)
            env2.seed(1337)
        except Exception as e:
            logger.trace(e)
            logger.error(f"Can not create second env. Soooory")
            env2 = None

    create_keys()

    run_loop()
