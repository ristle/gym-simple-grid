#!/usr/bin/env python3
# coding --utf-8--

import gym
from gym_simple_grid.helpers import ComplexGridEnv


class SimpleGrid8(ComplexGridEnv.ComplexGridEnv):
    def __init__(self):
        super().__init__(grid_size=(8, 8),
                         calc_astar_in_each_step=False,
                         num_obstacles=0,
                         dynamic_obstacles=False,
                         size_of_danger_zone_near_obstacles=0)


# Register new Simple Env
gym.register(
    id='SimpleGrid8-v2',
    entry_point='gym_simple_grid:SimpleGrid8'
)

