#!/usr/bin/env python3
# coding --utf-8--

import gym
from gym_simple_grid.helpers import ComplexGridEnv


class SimpleGrid20WithAStar(ComplexGridEnv.ComplexGridEnv):
    def __init__(self):
        super().__init__(grid_size=(16, 16),
                         calc_astar_in_each_step=True,
                         num_obstacles=0,
                         dynamic_obstacles=False,
                         size_of_danger_zone_near_obstacles=0,
                         reward_for_done_mission=15,
                         reward_for_dynamic_move=0.5,
                         reward_for_straight_move=3,
                         reward_for_minimal_distance=15,
                         penalty_for_moving_near_obstacles=2,
                         penalty_for_dynamic_move=0.5,
                         penalty_for_failure_mission=4,
                         penalty_for_straight_move=3,
                         penalty_for_moving_near_danger_zones=2,
                         penalty_for_moving_on_previous_path=5,
                         penalty_for_over_max_steps=100
                         )


# Register new Simple Env
gym.register(
    id='SimpleGrid20WithAStar-v2',
    entry_point='gym_simple_grid:SimpleGrid20WithAStar'
)
