#!/usr/bin/env python3
# coding --utf-8--


import gym
import numpy as np
from loguru import logger
from gym_simple_grid.helpers import Actions
from gym_simple_grid.helpers import ComplexGridEnv
from gym_simple_grid.helpers import AStarPathFinder


class SecondAgent(ComplexGridEnv.ComplexGridEnv):
    def __init__(self, target_image_size=400):
        self.grid = None
        self.moving = True
        self.agent_pose = None
        self.target_pose = None
        self.action_space = None
        self.second_agent = None
        self.observation_space = None
        self.target_image_size = target_image_size

        self.actions = Actions
        self.pose = np.array([4, 4])

        self.moving = True
        self.max_steps = 100
        self.grid = None
        self._target = [4, 4]
        self.i_step = 0
        self.history = list()
        self._grid_size = (10, 10)
        self.np_random, _ = gym.utils.seeding.np_random(1337)

        self.done = False

    def normal_init(self,
                    current_position,
                    action_space,
                    grid,
                    target_pose,
                    agent_pose,
                    grid_size=(16, 16)):
        super().__init__(grid_size=grid_size,
                         calc_astar_in_each_step=True,
                         num_obstacles=0,
                         dynamic_obstacles=False,
                         size_of_danger_zone_near_obstacles=0,
                         reward_for_done_mission=15,
                         reward_for_dynamic_move=0.5,
                         reward_for_straight_move=3,
                         reward_for_minimal_distance=15,
                         penalty_for_moving_near_obstacles=2,
                         penalty_for_dynamic_move=0.5,
                         penalty_for_failure_mission=4,
                         penalty_for_straight_move=3,
                         penalty_for_moving_near_danger_zones=2,
                         penalty_for_moving_on_previous_path=1,
                         penalty_for_over_max_steps=100
                         )
        self.moving = True
        self.grid = grid
        self._target = target_pose
        self.agent_pose = agent_pose
        self.position = current_position
        self.action_space = action_space

        self.i_step = 0
        self.max_steps = 100
        self.history = list()
        self.actions = Actions.Actions
        self._grid_size = (grid_size[0] + 2, grid_size[1] + 2)
        self.np_random, _ = gym.utils.seeding.np_random(1337)

        self.done = False
        image_observation_space = gym.spaces.Box(low=0, high=255, shape=(self._grid_size[1], self._grid_size[0], 3),
                                                 dtype='uint8')
        self.observation_space = gym.spaces.Dict({'image': image_observation_space})
        super().reset()


gym.register(
    id='SecondAgent-v1',
    entry_point='gym_simple_grid:SecondAgent'
)
