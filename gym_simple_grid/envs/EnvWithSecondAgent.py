#!/usr/bin/env python3
# coding --utf-8--

import cv2
import gym
import numpy as np
from loguru import logger
from gym_simple_grid.helpers import Actions
from gym_simple_grid.helpers import ComplexGridEnv


class EnvWithSecondAgent(ComplexGridEnv.ComplexGridEnv):
    def __init__(self):

        super().__init__(grid_size=(16, 16),
                         calc_astar_in_each_step=True,
                         num_obstacles=0,
                         dynamic_obstacles=False,
                         size_of_danger_zone_near_obstacles=0,
                         reward_for_done_mission=15,
                         reward_for_dynamic_move=0.5,
                         reward_for_straight_move=3,
                         reward_for_minimal_distance=15,
                         penalty_for_moving_near_obstacles=2,
                         penalty_for_dynamic_move=0.5,
                         penalty_for_failure_mission=4,
                         penalty_for_straight_move=3,
                         penalty_for_moving_near_danger_zones=2,
                         penalty_for_moving_on_previous_path=1,
                         penalty_for_over_max_steps=100
                         )

    def reset(self):
        super().reset()
        x, y = self._generate_pose()
        self.grid[y][x] = 254

        self.second_agent_pose[0] = x
        self.second_agent_pose[1] = y

    def __set_second_agent_pose(self, value: int):
        try:
            self.grid[self.second_agent_pose[1]][self.second_agent_pose[0]] = 0
        except Exception as e:
            logger.info(f"{e}")

    def step(self, action: Actions.Actions):
        self.__set_second_agent_pose(0)
        values = super().step(action)
        self.__set_second_agent_pose(255)

        return values

    def render(self, mode='human'):
        cell_size = self.target_image_size // max(self._grid_size)
        border_size = cell_size // self._grid_size[0] if cell_size > self._grid_size[0] else 1
        w, h = self._grid_size[0], self._grid_size[1]
        img = np.zeros([cell_size * h, cell_size * w, 3], np.uint8)
        for i in range(h):
            cv2.line(img, (0, i * cell_size), (cell_size * w, i * cell_size), [128, 128, 128], 1)
        for i in range(w):
            cv2.line(img, (i * cell_size, 0), (i * cell_size, cell_size * h), [128, 128, 128], 1)
        for i in range(w):
            for j in range(h):
                color = tuple(np.array([self._at([i, j])] * 3).tolist())
                cv2.rectangle(img, (i * cell_size + border_size, j * cell_size + border_size),
                              ((i + 1) * cell_size - border_size, (j + 1) * cell_size - border_size),
                              color, cv2.FILLED)

        i, j = self.position
        color = (0, 0, 255)
        cv2.rectangle(img, (i * cell_size + border_size, j * cell_size + border_size),
                      ((i + 1) * cell_size - border_size, (j + 1) * cell_size - border_size),
                      color, cv2.FILLED)
        i, j = self.second_agent_pose
        color = (255, 0, 0)
        cv2.rectangle(img, (i * cell_size + border_size, j * cell_size + border_size),
                      ((i + 1) * cell_size - border_size, (j + 1) * cell_size - border_size),
                      color, cv2.FILLED)
        i, j = self._target
        color = (0, 255, 0)
        cv2.rectangle(img, (i * cell_size + border_size, j * cell_size + border_size),
                      ((i + 1) * cell_size - border_size, (j + 1) * cell_size - border_size),
                      color, cv2.FILLED)
        for i in range(len(self.history) - 1):
            x0, y0 = self.history[i][0] * cell_size + cell_size // 2, self.history[i][1] * cell_size + cell_size // 2
            x1 = self.history[i + 1][0] * cell_size + cell_size // 2
            y1 = self.history[i + 1][1] * cell_size + cell_size // 2
            cv2.line(img, (x0, y0), (x1, y1), (0, 0, 255))

        for i in range(len(self.astar_path) - 1):
            x0 = self.astar_path[i][0] * cell_size + cell_size // 2
            y0 = self.astar_path[i][1] * cell_size + cell_size // 2
            x1 = self.astar_path[i + 1][0] * cell_size + cell_size // 2
            y1 = self.astar_path[i + 1][1] * cell_size + cell_size // 2
            cv2.line(img, (x0, y0), (x1, y1), (255, 0, 0))

        if mode == 'human':
            cv2.imshow("", img)
            cv2.waitKey()
        elif mode == 'rgb_array':
            return img
        else:
            super().render(mode)


# Register new Simple Env
gym.register(
    id='SimpleGrid20WithAStar-v1',
    entry_point='gym_simple_grid:SimpleGrid20WithAStar'
)
