#!/usr/bin/env python3
# coding --utf-8--

import gym
from gym_simple_grid.helpers import ComplexGridEnv


class EnvWith3DynamicObstacles8by8(ComplexGridEnv.ComplexGridEnv):
    def __init__(self):
        super().__init__(grid_size=(8, 8),
                         calc_astar=True,
                         calc_astar_in_each_step=True,
                         num_obstacles=3,
                         dynamic_obstacles=False,
                         size_of_danger_zone_near_obstacles=2)


class EnvWith3DynamicObstacles16by16(ComplexGridEnv.ComplexGridEnv):
    def __init__(self):
        super().__init__(grid_size=(16, 16),
                         calc_astar=True,
                         calc_astar_in_each_step=True,
                         num_obstacles=4,
                         dynamic_obstacles=True,
                         size_of_danger_zone_near_obstacles=2,
                         reward_for_done_mission=25,
                         reward_for_dynamic_move=0.7,
                         reward_for_straight_move=3,
                         reward_for_minimal_distance=20,
                         penalty_for_moving_near_obstacles=1,
                         penalty_for_dynamic_move=1.5,
                         penalty_for_failure_mission=5,
                         penalty_for_straight_move=3,
                         penalty_for_moving_near_danger_zones=1,
                         penalty_for_moving_on_previous_path=5,
                         penalty_for_over_max_steps=100
                         )


# Register new Simple Env
gym.register(
    id='3DynamicObstacles8by8-v1',
    entry_point='gym_simple_grid:EnvWith3DynamicObstacles8by8'
)

# Register new Simple Env
gym.register(
    id='EnvWith3DynamicObstacles16by16-v1',
    entry_point='gym_simple_grid:EnvWith3DynamicObstacles16by16'
)
