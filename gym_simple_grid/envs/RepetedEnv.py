#!/usr/bin/env python3
# coding --utf-8--
import math

import gym
import typing
import numpy as np
from loguru import logger
from gym_simple_grid.helpers import ComplexGridEnv


class RepeatedEnv(ComplexGridEnv.ComplexGridEnv):
    def __init__(self):
        self.current_pose = np.array([1, 1])
        self.target_pose = np.array([1, 1])
        self.last_history = list()
        self.history = list()
        self.i_steps = 0
        self.ideal_path_length = 0
        self.penalty_for_long_movement = 0.5
        self.sum_of_reward = 0

        self.reward = 0
        self.need_to_reset = True

        super().__init__(grid_size=(16, 16),
                         calc_astar=True,
                         calc_astar_in_each_step=True,
                         num_obstacles=4,
                         dynamic_obstacles=True,
                         size_of_danger_zone_near_obstacles=2,
                         reward_for_done_mission=25,
                         reward_for_dynamic_move=0.45,
                         reward_for_straight_move=3,
                         reward_for_minimal_distance=20,
                         penalty_for_moving_near_obstacles=1,
                         penalty_for_dynamic_move=1.5,
                         penalty_for_failure_mission=5,
                         penalty_for_straight_move=3,
                         penalty_for_moving_near_danger_zones=1,
                         penalty_for_moving_on_previous_path=10,
                         penalty_for_over_max_steps=100,
                         move_obstacle=False
                         )

    def __update_next_step(self):
        if len(self.history):
            self.current_pose = self.history[1 if len(self.history) > 1 else 0]

    def __update_temp_step(self):
        if len(self.history):
            self.current_pose = self.history[1 if len(self.history) > 1 else 0]
            self.position = self.history[1 if len(self.history) > 1 else 0]

    def __reset_all_env(self):
        self.reward = 0
        self.i_steps = 0
        self.sum_of_reward = 0
        self.current_pose = self.position
        self.target_pose = self._target

        return_info = super().reset()

        self.ideal_path_length = len(self.astar_path)
        return return_info

    def reset(self):
        if self.need_to_reset:
            self.need_to_reset = False
            return self.__reset_all_env()

        if not self.need_to_reset:
            obs = self._get_observation()
            obs, self.reward, self.need_to_reset, _ = self.__update_step_then_done(obs, self.reward)
            if self.need_to_reset:
                return self.reset()
            return obs

    def __update_step_then_done(self, obs, reward, _=None):
        if _ is None:
            _ = dict()

        self.i_steps += 1
        self._update_obstacle_position()

        return self.__process_system_state(obs, reward, _)

    def __process_step(self, obs, reward, _=None):
        if _ is None:
            _ = dict()

        reward = self._calculate_reward_for_smooth_path(reward)
        self.__update_temp_step()

        self.last_history = self.history
        self.history = list()
        self.i_step = 0

        return obs, reward, False, _

    def __process_done(self, reward: float) -> typing.Tuple[float, bool]:
        self.__update_next_step()

        self.last_history = self.history
        reward = self._calculate_reward_for_smooth_path(reward)
        done = True

        self.last_history = self.history
        self.history = list()
        self.i_step = 0

        return reward, done

    def __process_system_state(self, obs, reward, _):
        pose_weight = self._at(self.position)
        done = False
        if pose_weight == 64 and pose_weight == 254:
            return obs, reward, True, _
        elif self._dist(self.position, self._target) <= 1 and \
                self._dist(self.current_pose, self._target) >= 1:
            self.i_steps += 1
            obs, reward, done, _ = self.__process_step(obs, reward)
            return obs, reward, done, _
        elif self._dist(self.current_pose, self._target) <= 1:
            reward, done = self.__process_done(reward)
        return obs, reward, done, _

    def _calculate_reward_for_smooth_path(self, reward):
        min_size = min(len(self.history), len(self.last_history))
        # try:
        #     # difference = np.abs(np.array(self.history[:-min_size]) - np.array(self.last_history[:-min_size]))
        #     for index in range(0, min_size):
        #         point = self.last_history[index]
        #         found_in_history = self._find_pose_in_history(point, self.history)
        #         if self._at(point) == 3 and not found_in_history:
        #             reward -= 0.1
        #             # reward -= 5 * math.hypot(difference[i][0], difference[i][1])
        # except Exception as e:
        #     logger.error(e)
        #     logger.trace(e.__traceback__)
        return reward

    @staticmethod
    def _find_pose_in_history(pose: np.array, history: list[np.array]) -> bool:
        try:
            # This need to change type of positions into history from numpy array to list
            return pose.tolist() in np.array(history).tolist()
        except Exception as e:
            logger.error(f"{e}")
            return False

    def step(self, action: ComplexGridEnv.Actions.Actions):
        obs, reward, done, _ = super().step(action)
        if done:
            obs, reward, __, _ = self.__process_system_state(obs, reward, _)

        return obs, reward, done, _


# Register new Simple Env
gym.register(
    id='RepeatedEnv-v1',
    entry_point='gym_simple_grid:RepeatedEnv'
)
