#!/usr/bin/env python3
# coding --utf-8--

from enum import IntEnum
import numpy as np


class Actions(IntEnum):
    Up = 0
    UpRight = 1
    Right = 2
    DownRight = 3
    Down = 4
    DownLeft = 5
    Left = 6
    UpLeft = 7
    Stay = 8


def get_new_pose_from_action(pose: np.array, action: Actions) -> np.array:
    if action == Actions.Up:
        return pose + [0, -1]
    elif action == Actions.UpRight:
        return pose + [1, -1]
    elif action == Actions.Right:
        return pose + [1, 0]
    elif action == Actions.DownRight:
        return pose + [1, 1]
    elif action == Actions.Down:
        return pose + [0, 1]
    elif action == Actions.DownLeft:
        return pose + [-1, 1]
    elif action == Actions.Left:
        return pose + [-1, 0]
    elif action == Actions.UpLeft:
        return pose + [-1, -1]
    elif action == Actions.Stay:
        return pose
