#!/usr/bin/env python3
# coding --utf-8--

import cv2
import gym
import math
import typing
import numpy as np
from loguru import logger
from typing import Tuple, List
from gym_simple_grid.helpers import Actions
from gym_simple_grid.helpers import Obstacle
from gym_simple_grid.helpers import AStarPathFinder


class ComplexGridEnv(gym.Env):
    def __init__(self, grid_size: Tuple[int, int] = (8, 8),
                 start_position: Tuple[int, int] = None,
                 target: Tuple[int, int] = None,
                 obstacles: List[Tuple[int, int]] = None,
                 num_obstacles: int = 3,
                 max_steps: int = 100,
                 seed: int = 1337,
                 target_image_size=400,
                 dynamic_obstacles: bool = True,
                 complex_dynamic_obstacles: bool = True,
                 calc_astar: bool = True,
                 calc_astar_in_each_step: bool = True,
                 size_of_danger_zone_near_obstacles: int = 2,
                 complex_penalty_near_obstacle_danger_zone: bool = True,
                 reward_for_done_mission: float = 7.5,
                 reward_for_dynamic_move: float = 0.25,
                 reward_for_straight_move: float = 1,
                 reward_for_minimal_distance: float = 5,
                 penalty_for_dynamic_move: float = 0.25,
                 penalty_for_straight_move: float = 1,
                 penalty_for_moving_on_previous_path: float = 0.5,
                 penalty_for_moving_near_obstacles: float = 1.,
                 penalty_for_moving_near_danger_zones: float = 1.,
                 penalty_for_failure_mission: float = 2.0,
                 penalty_for_over_max_steps: float = 0.0,
                 move_obstacle: bool = True):
        """
                Args:
                    grid_size: Size of gym field, (w, h)
                    start_position: Initial position of agent in grid. If None, then randomized.
                                    (x, y), 0 < x < grid_size[0], 0 < y < grid_size[1]
                    target: Position of target in grid. If None, then randomized.
                            (x, y), 0 < x < grid_size[0], 0 < y < grid_size[1]
                    obstacles: Optional list of obstacles coordinates. If None then `num_obstacles` obstacles will be generated
                    num_obstacles: Optional number of obstacles. Used only if `obstacles` if None
                    max_steps: Maximum number of steps after which gym will be restarted
                    seed: Random generator seed
                    target_image_size: Size of the biggest side of rendered image
                """
        # Size of grid with borders
        self._grid_size = (grid_size[0] + 2, grid_size[1] + 2)

        self.grid = None
        self.done = None
        self.astar = None
        self.i_step = None
        self.history = None
        self.target = target
        self.gray_points = 0
        self.np_random = None
        self.astar_path = None
        self.num_obstacles = 0
        self.sum_of_reward = 0
        self.obstacles = obstacles
        self.max_steps = max_steps
        self.start_position = None
        self.astar_distance = None
        self.last_astar_path = list()
        self.actions = Actions.Actions
        self._target = np.array([2, 2])
        self.position = np.array([1, 1])
        self.target_pose = self._target
        self.current_pose = self.position
        self.target_image_size = target_image_size
        self.action_space = gym.spaces.Discrete(len(self.actions))

        self.calc_astar = calc_astar
        self.move_obstacle = move_obstacle
        self.dynamic_obstacles = dynamic_obstacles
        self.calc_astar_in_each_step = calc_astar_in_each_step
        self.reward_for_done_mission = reward_for_done_mission
        self.reward_for_dynamic_move = reward_for_dynamic_move
        self.reward_for_straight_move = reward_for_straight_move
        self.penalty_for_dynamic_move = penalty_for_dynamic_move
        self.complex_dynamic_obstacles = complex_dynamic_obstacles
        self.penalty_for_straight_move = penalty_for_straight_move
        self.penalty_for_over_max_steps = penalty_for_over_max_steps
        self.reward_for_minimal_distance = reward_for_minimal_distance
        self.penalty_for_failure_mission = penalty_for_failure_mission
        self.penalty_for_moving_near_obstacles = penalty_for_moving_near_obstacles
        self.size_of_danger_zone_near_obstacles = size_of_danger_zone_near_obstacles
        self.penalty_for_moving_on_previous_path = penalty_for_moving_on_previous_path
        self.penalty_for_moving_near_danger_zones = penalty_for_moving_near_danger_zones
        self.complex_penalty_near_obstacle_danger_zone = complex_penalty_near_obstacle_danger_zone

        image_observation_space = gym.spaces.Box(low=0, high=255,
                                                 shape=(self._grid_size[1], self._grid_size[0], 3),
                                                 dtype='uint8')
        self.observation_space = gym.spaces.Dict({'image': image_observation_space})

        self._assert_in_init(start_position, target, obstacles, num_obstacles)

        self.seed(seed=seed)

        self.second_agent_pose = self._random_position()
        self.reset()

    def _generate_pose(self) -> typing.Tuple:
        while True:
            x0 = np.random.randint(1, self._grid_size[0] - 1)
            y0 = np.random.randint(1, self._grid_size[1] - 1)
            if np.all(np.array([y0, x0]) == self.position) or \
                    np.all(np.array([y0, x0]) == self.target):
                continue
            break
        return x0, y0

    def _assert_in_init(self, start_position: np.array, target: np.array,
                        obstacles: List[Tuple[int, int]], num_obstacles: int):
        """
            Assert while creating our class in __init__
        """
        if start_position is not None:
            assert 0 <= start_position[0] < self.grid_size[0] and 0 <= start_position[1] < self.grid_size[1]
        self.start_position = start_position

        if target is not None:
            assert target != start_position
            assert 0 <= target[0] < self.grid_size[0] and 0 <= target[1] < self.grid_size[1]
        if obstacles is not None:
            assert num_obstacles is None, "If use `obstacles`, `num_obstacles` should be None"
            for x, y in obstacles:
                assert (x, y) != start_position and (x, y) != target
                assert 0 <= x < self.grid_size[0] and 0 <= y < self.grid_size[1]
            self.num_obstacles = len(obstacles)
        elif num_obstacles is not None:
            self.num_obstacles = num_obstacles

    @property
    def grid_size(self):
        return self._grid_size[0] - 2, self._grid_size[1] - 2

    def seed(self, seed=1337):
        # Seed the random number generator
        self.np_random, _ = gym.utils.seeding.np_random(seed)
        return [seed]

    def reset(self):
        if self.grid is not None:
            self.grid[:] = 255
        else:
            self.grid = np.ones((self._grid_size[1], self._grid_size[0]), np.uint8) * 255
        self.grid[0] = 0
        self.grid[-1] = 0
        self.grid[:, 0] = 0
        self.grid[:, -1] = 0

        if self.start_position is None:
            new_position = self._random_position()
            self.position[0] = new_position[0]
            self.position[1] = new_position[1]
        else:
            self.position = np.array([self.start_position[0] + 1, self.start_position[1] + 1])

        self.obstacles = list()
        if self.target is None:
            while 1:
                new_target = self._random_position()
                self._target[0] = new_target[0]
                self._target[1] = new_target[1]
                if np.any(self._target != self.position):
                    break
        else:
            self._target = np.array([self.target[0] + 1, self.target[1] + 1])
        self.grid[self._target[1], self._target[0]] = 255

        self.gray_points = 0
        num_obstacles = 0
        self.obstacles = list()
        while num_obstacles != self.num_obstacles:
            x0 = self.np_random.randint(1, self._grid_size[0] - 1)
            y0 = self.np_random.randint(1, self._grid_size[1] - 1)

            x1 = self.np_random.randint(1, self._grid_size[0] - 1)
            y1 = self.np_random.randint(1, self._grid_size[1] - 1)
            if np.all(np.array([x0, y0]) == self.position) or np.all(np.array([x0, y0]) == self.target) \
                    or np.all(np.array([x1, y1]) == self.position) or np.all(np.array([x1, y1]) == self.target):
                continue
            self.obstacles.append(Obstacle.Obstacle(x0, y0, x1, y1,
                                                    grid_size=self._grid_size[0],
                                                    dynamic=self.dynamic_obstacles,
                                                    complex_dynamic=self.complex_dynamic_obstacles))
            self.grid[y0, x0] = 0
            num_obstacles += 1

        self.done = False
        self.history = [self.position]

        if self.calc_astar:
            self.astar = AStarPathFinder.AStarPathFinder(self.grid)
            self.last_astar_path = list()
            self.astar_path = list(self.astar.astar(tuple(self.position.tolist()), tuple(self._target.tolist())))

        self.i_step = 0
        self.sum_of_reward = 0

        self._draw_obstacle_circle()
        obs = self._get_observation()
        return obs

    @staticmethod
    def _get_circle_points(radius: int, n: int = 50):
        """
            Get set of unique discrete points  which fits in circle
        """
        return set([(int(math.cos(2 * math.pi / n * x) * radius),
                     int(math.sin(2 * math.pi / n * x) * radius))
                    for x in range(0, n + 1)])

    def _draw_obstacle_circle(self):
        gray_actions_ = ComplexGridEnv._get_circle_points(self.size_of_danger_zone_near_obstacles)
        for obstacle in self.obstacles:
            new_pose = obstacle.get_pose()
            self.grid[new_pose[1], new_pose[0]] = 0
            for action_ in gray_actions_:
                n_pose = new_pose[0] + action_[0], new_pose[1] + action_[1]
                n_pose = np.clip(0, self._grid_size[0] - 1, n_pose)
                self.grid[n_pose[1], n_pose[0]] = 64 if self.grid[n_pose[1], n_pose[0]] != 0 else 0

    def _update_obstacle_position(self):
        gray_actions_ = ComplexGridEnv._get_circle_points(self.size_of_danger_zone_near_obstacles)

        for obstacle in self.obstacles:
            pose = obstacle.get_pose()
            self.grid[pose[1], pose[0]] = 255
            for action_ in gray_actions_:
                n_pose = np.array([pose[0] + action_[0], pose[1] + action_[1]])
                n_pose = np.clip(0, self._grid_size[0] - 1, n_pose)
                self.grid[n_pose[1], n_pose[0]] = 255 \
                    if self.grid[n_pose[1], n_pose[0]] != 0 else 0
            obstacle.step()

        self._draw_obstacle_circle()

    def _astar_reward_in_each_step(self, reward: float, action: Actions.Actions) -> float:
        try:
            self.last_astar_path = self.astar_path
            self.astar = AStarPathFinder.AStarPathFinder(self.grid)
            self.astar_path = list(self.astar.astar(tuple(self.position.tolist()), tuple(self._target.tolist())))

            def unit_vector(vector):
                """ Returns the unit vector of the vector.  """
                return vector / np.linalg.norm(vector)

            def angle_between(vec_1: np.array, vec_2: np.array) -> float:
                vec_1 = unit_vector(vec_1)
                vec_2 = unit_vector(vec_2)
                return np.arccos(np.clip(np.dot(vec_1, vec_2), -1.0, 1.0))

            angle_first = unit_vector(self.history[-1] - self.history[-2]
                                      if len(self.history) > 1 else np.array([0, 1]))
            if len(self.history) > 1 and len(self.last_astar_path) > 2:
                angle_second = np.clip(a_min=-1., a_max=1.0, a=np.array(self.last_astar_path[2]) - self.history[-2])
            elif len(self.astar_path) > 2:
                angle_second = np.clip(a_min=-1., a_max=1.0, a=np.array(self.position - self.astar_path[0]))
            else:
                angle_second = np.array([0, 1])

            angle = angle_between(angle_first, angle_second)
            if (abs(angle) < 1e-32 or angle % math.pi == 2 or angle % math.pi == 0.0) and abs(
                    abs(angle) - math.pi) > 1e-2:
                reward += self.reward_for_straight_move
            elif abs(abs(angle) - np.pi / 4) <= 0.1 and abs(angle) > 1e-32:
                reward += self.reward_for_dynamic_move
            elif abs(abs(angle) - np.pi * 3 / 4) <= 0.1 and abs(angle) > 1e-32:
                reward -= self.penalty_for_dynamic_move
            elif abs(abs(angle) - math.pi) < 1e-2:
                reward -= self.reward_for_straight_move
            else:
                reward -= self.reward_for_straight_move
        except Exception as e:
            logger.error(f"{e}")
            logger.trace(e)

        return reward

    def _find_in_history(self) -> bool:
        try:
            # This need to change type of positions into history from numpy array to list
            return self.position.tolist() in np.array(self.history[:-2]).tolist()
        except:
            return False

    def step(self, action: Actions.Actions):
        self.i_step += 1
        new_position = Actions.get_new_pose_from_action(self.position, action)
        new_position = np.clip(a_max=self._grid_size[1]-1, a_min=0, a=new_position)

        self.position = new_position
        self.history.append(new_position)

        reward = 0
        done = False

        if self.move_obstacle:
            self._update_obstacle_position()

        # Достигли цели
        if np.all(new_position == self._target):
            d = self._calculate_distance(self.history)  # Расчитываем длину пройденного пути
            min_distance = np.linalg.norm(self.history[0] - self._target)
            reward = self.reward_for_minimal_distance * min_distance / d if d != 0 else 1
            reward += self.reward_for_done_mission
            reward -= self.gray_points / max(self._grid_size[0], self._grid_size[1])
            for pose in np.unique(np.array(self.history).view('i,i')):
                count_of_times = np.sum(self.history == pose)
                if count_of_times > 1:
                    reward -= count_of_times * self.penalty_for_moving_on_previous_path
            done = True
            # Врезались в непроходимое препятствие
        elif self._at(new_position) == 0:
            reward = - self.penalty_for_failure_mission
            done = True
        elif self._at(new_position) == 64:
            self.gray_points += 1
            reward = - self.penalty_for_moving_near_danger_zones
        # Превысили допустимое количество шагов
        elif self.i_step >= self.max_steps:
            reward -= self.penalty_for_over_max_steps
            done = True
        self.done = done

        if self.calc_astar_in_each_step:
            reward = self._astar_reward_in_each_step(reward, action)

        if len(self.history) > 1 and self._find_in_history():
            reward -= self.penalty_for_moving_on_previous_path

        reward = self._update_obstacles_pose(reward, new_position)

        obs = self._get_observation()
        if done:
            reward = reward + self.sum_of_reward if self.sum_of_reward < 0 else reward
        else:
            self.sum_of_reward += reward

        return obs, reward, done, {}

    def _update_obstacles_pose(self, reward: int, new_position: np.array):
        for obstacle in self.obstacles:
            if np.linalg.norm(obstacle.get_pose() - new_position) <= 1:
                reward -= self.penalty_for_moving_near_danger_zones

            for radius in range(1, self.size_of_danger_zone_near_obstacles + 1):
                if np.linalg.norm(obstacle.get_pose() - new_position) == radius and \
                        self.complex_penalty_near_obstacle_danger_zone:
                    reward -= self.penalty_for_moving_near_danger_zones / (2. * radius)
        return reward

    def render(self, mode='human') -> np.array:
        cell_size = self.target_image_size // max(self.grid_size)
        border_size = cell_size // self._grid_size[0] if cell_size > self._grid_size[0] else 1

        w, h = self._grid_size[0], self._grid_size[1]
        img = np.zeros([cell_size * h, cell_size * w, 3], np.uint8)

        for i in range(h):
            cv2.line(img, (0, i * cell_size), (cell_size * w, i * cell_size), [128, 128, 128], 1)
        for i in range(w):
            cv2.line(img, (i * cell_size, 0), (i * cell_size, cell_size * h), [128, 128, 128], 1)
        for i in range(w):
            for j in range(h):
                color = np.array([self._at([i, j])] * 3).tolist()
                cv2.rectangle(img, (i * cell_size + border_size, j * cell_size + border_size),
                              ((i + 1) * cell_size - border_size, (j + 1) * cell_size - border_size),
                              color, cv2.FILLED)

        i, j = self.position
        color = (0, 0, 255)
        cv2.rectangle(img, (i * cell_size + border_size, j * cell_size + border_size),
                      ((i + 1) * cell_size - border_size, (j + 1) * cell_size - border_size),
                      color, cv2.FILLED)
        i, j = self._target
        color = (0, 255, 0)
        cv2.rectangle(img, (i * cell_size + border_size, j * cell_size + border_size),
                      ((i + 1) * cell_size - border_size, (j + 1) * cell_size - border_size),
                      color, cv2.FILLED)
        for i in range(len(self.history) - 1):
            x0, y0 = self.history[i][0] * cell_size + cell_size // 2, self.history[i][
                1] * cell_size + cell_size // 2
            x1 = self.history[i + 1][0] * cell_size + cell_size // 2
            y1 = self.history[i + 1][1] * cell_size + cell_size // 2
            cv2.line(img, (x0, y0), (x1, y1), (0, 0, 255))

        for i in range(len(self.astar_path) - 1):
            x0 = self.astar_path[i][0] * cell_size + cell_size // 2
            y0 = self.astar_path[i][1] * cell_size + cell_size // 2
            x1 = self.astar_path[i + 1][0] * cell_size + cell_size // 2
            y1 = self.astar_path[i + 1][1] * cell_size + cell_size // 2
            cv2.line(img, (x0, y0), (x1, y1), (255, 0, 0))

        if mode == 'human':
            cv2.imshow("", img)
            cv2.waitKey(0)
        elif mode == 'rgb_array':
            return img
        else:
            super().render(mode)

    def _at(self, position) -> float:
        return self.grid[position[1], position[0]]

    def _random_position(self) -> np.array:
        return np.array([self.np_random.randint(1, self._grid_size[0] - 1),
                         self.np_random.randint(1, self._grid_size[1] - 1)], np.int64)

    def _get_observation(self) -> dict:
        agent = np.zeros_like(self.grid)
        agent[self.position[1], self.position[0]] = 255
        target = np.zeros_like(self.grid)
        target[self._target[1], self._target[0]] = 255
        img = np.stack([agent, target, self.grid], -1)
        obs = {
            'image': img,
            'mission': '',
        }
        return obs

    def _calculate_distance(self, path) -> float:
        d = 0
        for i in range(len(path) - 1):
            p0 = path[i]
            p1 = path[i + 1]
            d += self._dist(p0, p1)
        return d

    @staticmethod
    def _dist(p0, p1) -> float:
        return math.hypot(p0[0] - p1[0], p0[1] - p1[1])
