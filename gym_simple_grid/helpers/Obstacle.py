#!/usr/bin/env python3
# coding --utf-8--

from gym_simple_grid.helpers import AStarPathFinder
from gym_simple_grid.helpers import Actions
import numpy as np


class Obstacle:
    def __init__(self, x, y, x_t, y_t,
                 time_of_life=5000, grid_size=10,
                 dynamic: bool = True, complex_dynamic: bool = True):
        self.pose = np.array([x, y])

        self.complex_dynamic = complex_dynamic
        self.timeOfLife = time_of_life
        self.gridSize = grid_size
        self.dynamic = dynamic

        self.grid = np.ones((self.gridSize, self.gridSize), np.uint8) * 255
        self.grid[0] = 0
        self.grid[-1] = 0
        self.grid[:, 0] = 0
        self.grid[:, -1] = 0

        self.target = x_t, y_t
        self.cycleOfLife = 0

        self.final_path = list()

        if self.complex_dynamic:
            self.astar = AStarPathFinder.AStarPathFinder(self.grid)
            self.astar_path = list(self.astar.astar(tuple(self.pose), tuple(self.target)))
            self.final_path = self.astar_path.copy()

    def update_pose(self, pose: np.array):
        self.pose = pose
        self.final_path = list()
        if self.complex_dynamic:
            self.astar = AStarPathFinder.AStarPathFinder(self.grid)
            self.astar_path = list(self.astar.astar(tuple(self.pose), tuple(self.target)))
            self.final_path = self.astar_path.copy()

    def get_pose(self) -> np.array:
        return self.pose

    def __get_new_pose_from_path(self) -> np.array:
        return self.astar_path.pop(0)

    def step(self):
        if self.dynamic or self.complex_dynamic:
            if self.complex_dynamic:
                if len(self.astar_path) > 0:
                    new_pose = self.__get_new_pose_from_path()
                    self.pose = np.clip(0, self.gridSize - 1, new_pose)
                    return self.pose
                else:
                    return self.pose
            else:
                # Get max value of the Action dynamically. So you won't need to change this code. if you were changed
                # number of action
                random_action = Actions.Actions(np.random.randint(max(Actions.Actions) + 1))
                self.pose = np.clip(0, self.gridSize - 1, Actions.get_new_pose_from_action(self.pose, random_action))
                self.final_path.append(self.pose)
        else:
            return self.pose
