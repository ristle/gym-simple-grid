#!/usr/bin/env python3
# coding --utf-8--

from astar import AStar
import numpy as np


class AStarPathFinder(AStar):
    def __init__(self, grid):
        self.grid = grid

    def neighbors(self, node):
        neighbours_offsets = [
            [-1, -1],
            [0, -1],
            [1, -1],
            [1, 0],
            [1, 1],
            [0, 1],
            [-1, 1],
            [-1, 0],
        ]
        neighbours = [(node[0] + offset[0], node[1] + offset[1]) for offset in neighbours_offsets]
        return neighbours

    def distance_between(self, n1, n2):
        d = np.linalg.norm(np.array(n1) - np.array(n2))
        h, w = self.grid.shape
        if n2[0] >= w or n2[1] >= h or self.grid[n2[1], n2[0]] == 0:
            d = 255
        if n2[0] >= w or n2[1] >= h or self.grid[n2[1], n2[0]] == 64:
            d = 255

        return d

    def heuristic_cost_estimate(self, current, goal):
        return np.linalg.norm(np.array(goal) - current)

    def is_goal_reached(self, current, goal):
        return current == goal
