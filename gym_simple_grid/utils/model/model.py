#!/usr/bin/env python3
# coding --utf-8--

import torch
import torch.nn as nn
import torch.nn.functional as F

import torch_ac

from torchvision.models.resnet import resnet18
from torch.distributions.categorical import Categorical


# Function from https://github.com/ikostrikov/pytorch-a2c-ppo-acktr/blob/master/model.py
def init_params(m):
    classname = m.__class__.__name__
    if classname.find("Linear") != -1:
        m.weight.data.normal_(0, 1)
        m.weight.data *= 1 / torch.sqrt(m.weight.data.pow(2).sum(1, keepdim=True))
        if m.bias is not None:
            m.bias.data.fill_(0)


class ACModel(nn.Module, torch_ac.RecurrentACModel):
    def __init__(self, obs_space, action_space, use_memory=False, use_text=False):
        super().__init__()

        # Decide which components are enabled
        self.use_text = use_text
        self.use_memory = use_memory

        self.mode = 'conv'
        # self.mode = 'fc'
        # self.mode = 'resnet'

        h, w, c = obs_space["image"]
        # Define image embedding
        if self.mode == 'conv':
            self.image_conv = nn.Sequential(
                nn.Conv2d(c, 16, (3, 3), (1, 1), (1, 1)),
                nn.BatchNorm2d(16),
                nn.ReLU(),
                # nn.MaxPool2d((2, 2)),
                nn.Conv2d(16, 32, (3, 3), (2, 2), (1, 1)),
                nn.BatchNorm2d(32),
                nn.ReLU(),
                nn.Conv2d(32, 128, (3, 3)),
                nn.BatchNorm2d(128),
                nn.ReLU()
            )
            self.image_embedding_size = 128
        elif self.mode == 'fc':
            self.image_conv = nn.Sequential(
                nn.Linear(h * w * c, 256),
                nn.ReLU(),
                nn.Linear(256, 128),
                nn.ReLU()
            )
            self.image_embedding_size = 128
        elif self.mode == 'resnet':
            self.resnet = resnet18(pretrained=False)
            self.resnet.conv1 = nn.Conv2d(
                c,
                self.resnet.conv1.out_channels,
                kernel_size=self.resnet.conv1.kernel_size,
                stride=self.resnet.conv1.stride,
                padding=self.resnet.conv1.padding,
                bias=False,
            )
            self.image_embedding_size = 512

        # Define memory
        if self.use_memory:
            self.memory_rnn = nn.LSTMCell(self.image_embedding_size, self.semi_memory_size)

        # Define text embedding
        if self.use_text:
            self.word_embedding_size = 32
            self.word_embedding = nn.Embedding(obs_space["text"], self.word_embedding_size)
            self.text_embedding_size = 128
            self.text_rnn = nn.GRU(self.word_embedding_size, self.text_embedding_size, batch_first=True)

        # Resize image embedding
        self.embedding_size = self.semi_memory_size
        if self.use_text:
            self.embedding_size += self.text_embedding_size

        # Define actor's model
        self.actor = nn.Sequential(
            nn.Linear(self.embedding_size, 64),
            nn.Tanh(),
            nn.Linear(64, action_space.n)
        )

        # Define critic's model
        self.critic = nn.Sequential(
            nn.Linear(self.embedding_size, 64),
            nn.Tanh(),
            nn.Linear(64, 1)
        )

        # Initialize parameters correctly
        self.apply(init_params)

    @property
    def memory_size(self):
        return 2*self.semi_memory_size

    @property
    def semi_memory_size(self):
        return self.image_embedding_size

    def forward(self, obs, memory):
        x = obs.image.transpose(1, 3).transpose(2, 3)
        x /= 255

        if self.mode == 'conv':
            x = self.image_conv(x)
            x = x.mean(-1).mean(-1)
        elif self.mode == 'fc':
            x = x.reshape(x.shape[0], -1)
        elif self.mode == 'resnet':
            x = self.resnet.conv1(x)
            x = self.resnet.bn1(x)
            x = self.resnet.relu(x)
            x = self.resnet.maxpool(x)
            x = self.resnet.layer1(x)
            x = self.resnet.layer2(x)
            x = self.resnet.layer3(x)
            x = self.resnet.layer4(x)
            x = self.resnet.avgpool(x)
            x = x.mean(-1).mean(-1)

        if self.use_memory:
            hidden = (memory[:, :self.semi_memory_size], memory[:, self.semi_memory_size:])
            hidden = self.memory_rnn(x, hidden)
            embedding = hidden[0]
            memory = torch.cat(hidden, dim=1)
        else:
            embedding = x

        if self.use_text:
            embed_text = self._get_embed_text(obs.text)
            embedding = torch.cat((embedding, embed_text), dim=1)

        x = self.actor(embedding)
        dist = Categorical(logits=F.log_softmax(x, dim=1))

        x = self.critic(embedding)
        value = x.squeeze(1)

        return dist, value, memory

    def _get_embed_text(self, text):
        _, hidden = self.text_rnn(self.word_embedding(text))
        return hidden[-1]
