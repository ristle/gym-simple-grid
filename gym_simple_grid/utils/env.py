#!/usr/bin/env python3
# coding --utf-8--

import gym
import gym_simple_grid


def make_env(env_key, seed=None):
    env = gym.make(env_key)
    env.seed(seed)
    return env
